﻿using System.Linq;

namespace InstaTest.Helpers
{
    public class ActionHelper : BaseHelper
    {
        private ApplicationManager _manager { get; set; }
        #region constants

        public string PutHearsClassName =>
            _manager.DataHelper.GetData("Settings", "InstagramData", "PutHearsClassName");

        public string HearsClassName => _manager.DataHelper.GetData("Settings", "InstagramData", "HearsClassName");
        protected const string path = @"C:\Users\EVG\Documents\FMS\InstaTests\InstaTests\bin\Debug\netcoreapp2.1";

        protected const string InstagramAuthUrl =
            "https://www.instagram.com/accounts/login/?hl=ru&source=auth_switcher";

        protected const string InstagramBaseUrlAfterAuth = "https://www.instagram.com/?hl=ru";

        #endregion

        public ActionHelper(ApplicationManager manager) : base(manager)
        {
            _manager = manager;
        }

        public void OpenPhoto()
        {
            var s = _webDriver.Await(x => x.GetElementsByClassName("_9AhH0"));

            _webDriver.Await(x => x.GetElementsByClassName("_9AhH0"))
                .First()
                .Click();
        }

        public void DoLike()

        {
            _webDriver.Await(xx =>
                xx.GetElementByClassName(HearsClassName)).Click();
        }

        public bool PhotoLiked => _webDriver.Await(xx => xx.GetElementByClassName(PutHearsClassName) != null);
    }
}