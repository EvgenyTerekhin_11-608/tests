﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using NUnit.Framework;

namespace InstaTest.Helpers
{
    /*[Flags]
    enum AuthPageUrl
    {
        [EnumMember(Value = "https://www.instagram.com/?hl=ru")]
        InstagramAuthUrl_1 = 0x2,

        [EnumMember(Value = "https://www.instagram.com")]
        InstagramAuthUrl_2 = 0x4
    }*/

    public class LoginHelper : BaseHelper
    {
        #region constants

        public const string PutHearsClassName = @"glyphsSpriteHeart__filled__24__red_5";
        public const string HearsClassName = @"glyphsSpriteHeart__outline__24__grey_9";
        protected const string path = @"C:\Users\EVG\Documents\FMS\InstaTests\InstaTests\bin\Debug\netcoreapp2.1";


        protected const string InstagramBaseUrlAfterAuth_1 = "https://www.instagram.com/?hl=ru";
        protected const string InstagramBaseUrlAfterAuth_2 = "https://www.instagram.com/";
        protected const string InstagramBaseUrlAfterAuth_3 = "https://www.instagram.com/brager17/";

        #endregion

        private ApplicationManager _manager { get; set; }

        public LoginHelper(ApplicationManager manager) : base(manager)
        {
            _manager = manager;
        }

        public void Auth()
        {
            var contactInfo = _manager.ContactHelper.BaseInfo;
            var credElemens = _webDriver.Await(x => x.GetElementsByClassName("_2hvTZ"));

            credElemens[0].SendKeys(contactInfo.Login);
            credElemens[1].SendKeys(contactInfo.Password);

            _webDriver.GetElementsByClassName("_0mzm-")[1].Click();
            Stopwatch sw = new Stopwatch();
            _webDriver.Await(x => constainsPredicate(x.Url));
        }

        public void LogOut()
        {
            _manager.NavigationHelper.NavigateToAccountPage();

            var elements1 =
                _webDriver.Await(x => x.GetElementsByClassName("glyphsSpriteSettings__outline__24__grey_9"));
            elements1[0].Click();

            Thread.Sleep(1000);
            var elements2 = _webDriver.Await(x => x.GetElementsByClassName("aOOlW"));
            elements2[5].Click();
        }

        public bool IsLogged(string login)
        {
            _manager.NavigationHelper.NavigateToAccountPage();
            Thread.Sleep(1000);
            var predicate = _webDriver.GetElementsByClassName("_9AhH0").Count != 0;
            return IsAuth && predicate;
        }

        public bool IsAuth => _webDriver.Await(x => constainsPredicate(x.Url));

        private readonly Func<string, bool> constainsPredicate =
            str => new[] {InstagramBaseUrlAfterAuth_1, InstagramBaseUrlAfterAuth_2, InstagramBaseUrlAfterAuth_3}
                .Contains(str);
    }
}