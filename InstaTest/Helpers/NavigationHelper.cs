﻿using System;
using System.Runtime.Serialization;

namespace InstaTest.Helpers
{
   
    public class NavigationHelper : BaseHelper
    {
        private string InstagramAuthUrl => _manager.ContactHelper.BaseInfo.BaseUrl;

        private  string InstagramPhotoList => _manager.DataHelper.GetData("Settings","InstagramData", "InstagramPhotoList");

        private string InstagramAccountPageUrl => _manager.DataHelper.GetData("Settings","InstagramData", "InstagramAccountPageUrl");

        private ApplicationManager _manager { get; set; }
        private string _baseUrl { get; set; }

        public NavigationHelper(ApplicationManager manager, string baseUrl) : base(manager)
        {
            _manager = manager;
            _baseUrl = baseUrl;
            Console.WriteLine(InstagramPhotoList);
        }

        public void Navigate(string url = "")
            => _webDriver.GoToUrl(url);

        public void NavigateToAuthUrl()
            => _webDriver.GoToUrl(InstagramAuthUrl);

        public void NavigateToPhotoList()
            => _webDriver.GoToUrl(InstagramPhotoList);

        public void NavigateToAccountPage()
            => _webDriver.GoToUrl(InstagramAccountPageUrl);
    }
}