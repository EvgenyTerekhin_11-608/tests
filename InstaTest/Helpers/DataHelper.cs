﻿namespace InstaTest.Helpers
{
    public class DataHelper:BaseHelper
    {
        private XmlParser _xmlParser { get; set; }
        protected const string path = @"C:\Users\EVG\Documents\FMS\InstaTests\InstaTests\bin\Debug\netcoreapp2.1\Settings.xml";

        public DataHelper(ApplicationManager manager) : base(manager)
        {
            _xmlParser = new XmlParser(path);
        }

        public string GetData(params string[] path) => _xmlParser.getData(path);
    }
}