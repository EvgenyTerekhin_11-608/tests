﻿using System;
using System.Text;
using NUnit.Framework;
using OpenQA.Selenium;

namespace InstaTest.Tests
{
    public class KatalonTestBase
    {
        protected ApplicationManager AppManager;
        private StringBuilder verificationErrors { get; set; }

        public KatalonTestBase()
        {
        }

        [SetUp]
        public void SetupTest()
        {
            AppManager = ApplicationManager.GetInstance();
            AppManager.NavigationHelper.NavigateToAuthUrl();

        }

    }
}