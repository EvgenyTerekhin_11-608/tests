﻿using System.Linq;
using NUnit.Framework;

namespace InstaTest.Tests
{
    [TestFixture]
    public class KatalonLikeTests : KatalonAuthTestBase
    {
        [Test]
        public void TestCase()
        {
            AppManager.NavigationHelper.NavigateToPhotoList();
            AppManager.ActionHelper.OpenPhoto();
            AppManager.ActionHelper.DoLike();
            AppManager.NavigationHelper.NavigateToPhotoList();
            AppManager.ActionHelper.OpenPhoto();


            Assert.True(AppManager.ActionHelper.PhotoLiked);
        }

        #region helpers

        #endregion
    }
}