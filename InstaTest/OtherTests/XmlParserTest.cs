﻿using InstaTest.Helpers;
using NUnit.Framework;

namespace InstaTest.OtherTests
{
    [TestFixture]
    public class XmlParserTest
    {
        private XmlParser _parser { get; set; }

        public XmlParserTest()
        {
            //замегить на свой путь
            _parser = new XmlParser(
                @"C:\Users\EVG\Documents\FMS\InstaTests\InstaTests\bin\Debug\netcoreapp2.1\Settings.xml");
        }

        [Test]
        public void getLogin_Test_brager17()
        {
            Assert.AreEqual(_parser.getLogin, "brager17");
        }

        public void getBaseUrl_Test_brager17()
        {
            Assert.AreEqual(_parser.getBaseUrl, "https://www.instagram.com/accounts/login/?source=auth_switcher");
        }

        public void getPassword_Test_brager17()
        {
            Assert.AreEqual(_parser.getPassword, "brager17");
        }
        [Test]
        public void SettingsTest()
        {
            Assert.AreEqual(_parser.getData("Settings","InstagramData", "InstagramPhotoList"), "https://www.instagram.com/explore/locations/221630742/kazan-tatarstan/?hl=ru");
        }
    }
}