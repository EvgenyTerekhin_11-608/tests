﻿using System.IO;

namespace InstaTest.Helpers
{
    public class BaseInfo
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string BaseUrl { get; set; }
    }

    public class ContactInfoCreater
    {
        private XmlParser _xmlParser { get; set; }

        private const string pathToXml =
            @"C:\Users\EVG\Documents\FMS\InstaTests\InstaTests\bin\Debug\netcoreapp2.1\Settings.xml";

        public ContactInfoCreater()
        {
            _xmlParser = new XmlParser(pathToXml);
        }

        public BaseInfo Create()
            =>
                new BaseInfo
                {
                    Login = _xmlParser.getLogin,
                    Password = _xmlParser.getPassword,
                    BaseUrl = _xmlParser.getBaseUrl
                };
    }
}