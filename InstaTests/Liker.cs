﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using OpenQA.Selenium.Chrome;

namespace InstaTests
{
    public class Liker
    {
        private const string InstagramAuthUrl = "https://www.instagram.com/accounts/login/?hl=ru&source=auth_switcher";
        private const string InstagramBaseUrlAfterAuth = "https://www.instagram.com/?hl=ru";

        public static void Like()
        {
            var c = new Credential();

            var browser = new ChromeDriver(Directory.GetCurrentDirectory());
            browser.Manage().Window.Maximize();

            browser.GoToUrl(InstagramAuthUrl);

            var credElemens = browser.Await(x => x.GetElementsByClassName("_2hvTZ"));

            credElemens[0].SendKeys(c.UserName);
            credElemens[1].SendKeys(c.Password);

            browser.GetElementsByClassName("_0mzm-")[1].Click();

            browser.Await(x => x.Url == InstagramBaseUrlAfterAuth);


            while (true)
            {
                browser.GoToUrl("https://www.instagram.com/explore/locations/221630742/kazan-tatarstan/?hl=ru");
                browser.Await(x => x.GetElementsByClassName("_9AhH0"))
                    .Skip(9)
                    .ToList()
                    .ForEach(x =>
                    {
                        try
                        {
                            x.Click();
                            var photo = browser.Await(xx =>
                                xx.GetElementByClassName("glyphsSpriteHeart__outline__24__grey_9"));
                            photo.Click();
                            browser.GetElementByClassName("ckWGn").Click();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                    });
                
                Thread.Sleep(277 * 1000);
            }
        }
    }
}